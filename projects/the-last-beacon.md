---
layout: project
type: project
image: images/tlb-sky.png
title: The Last Beacon
permalink: projects/the-last-beacon
date: 2014-04-26
summary: A game made for Ludum Dare 29.
---

<div class="ui small rounded images">

</div>
todo
A logic platformer game I originally made for Ludum Dare #28 back in 2014 and constantly improve since 2019. Created with Love2D (Lua framework). Currently inactive project, but I do make edits on it every once in a while.

(todo more info)

[Repository](https://gitlab.com/synthwavezoltan/the-last-beacon).
